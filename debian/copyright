Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libpdb-redo
Upstream-Contact: Maarten L. Hekkelman <maarten@hekkelman.com>
Source: http://github.com/PDB-REDO/libpdb-redo

Files: *
Copyright: © 2020 NKI/AVL, Netherlands Cancer Institute
License: BSD-2-Clause

Files: debian/*
Copyright: © 2020 NKI/AVL, Netherlands Cancer Institute
License: BSD-2-Clause

Files: examples/1cbs_map.mtz
Copyright: © 2016, Kleywegt, G.J, Bergfors, T, Jones, T.A
License: PDBe
Comment: Reflection data in mtz format, downloaded from PDBe website
 PDB ID: 1cbs, available at: http://www.rcsb.org

License: PDBe
 PDBe File License - as described in EBI PDBe site - May 25, 2018
 .
 The online data services and databases of EMBL-EBI are generated in part from
 data contributed by the community who remain the data owners.
 .
 When you contribute scientific data to a database through our website or
 other submission tools this information will be released at a time and in
 a manner consistent with the scientific data and we may store it permanently.
 .
 EMBL-EBI itself places no additional restrictions on the use or
 redistribution of the data available via its online services other than those
 provided by the original data owners.
 .
 EMBL-EBI does not guarantee the accuracy of any provided data, generated
 database, software or online service nor the suitability of databases,
 software and online services for any purpose.
 .
 The original data may be subject to rights claimed by third parties,
 including but not limited to, patent, copyright, other intellectual
 property rights, biodiversity-related access and benefit-sharing rights.
 For the specific case of the EGA database and human data consented for
 biomedical research, these rights may be formalised in Data Access
 Agreements. It is the responsibility of users of EMBL-EBI services to
 ensure that their exploitation of the data does not infringe any of the
 rights of such third parties.
 .
 License's text is available on EBI PDBe site (https://www.ebi.ac.uk), on page
 named "EMBL-EBI Terms of Use".
 URL: https://www.ebi.ac.uk/about/terms-of-use/

License: BSD-2-Clause
 Copyright (c) 2020 NKI/AVL, Netherlands Cancer Institute
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
