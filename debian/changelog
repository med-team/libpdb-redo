libpdb-redo (3.2.1-2) unstable; urgency=medium

  * update package name due to changed SONAME

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Wed, 29 Jan 2025 11:58:38 +0100

libpdb-redo (3.2.1-1) unstable; urgency=medium

  * New upstream version 3.2.1

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Tue, 28 Jan 2025 16:20:25 +0100

libpdb-redo (3.1.5-4) unstable; urgency=medium

  * Closes: #1046388, #1085426

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Tue, 29 Oct 2024 07:33:57 +0100

libpdb-redo (3.1.5-2) unstable; urgency=medium

  * Create a new upload.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 25 Oct 2024 14:36:33 +0200

libpdb-redo (3.1.5-1) unstable; urgency=medium

  * New upstream

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 25 Oct 2024 14:11:00 +0200

libpdb-redo (3.1.3-2) unstable; urgency=medium

  * Update libcifpp dependency.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 25 Oct 2024 11:36:18 +0200

libpdb-redo (3.1.3-1) unstable; urgency=medium

  * New upstream.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 18 Oct 2024 09:41:32 +0200

libpdb-redo (3.0.5-2) unstable; urgency=medium

  * Fix libcifpp dependency.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 03 Feb 2023 09:22:45 +0100

libpdb-redo (3.0.5-1) unstable; urgency=medium

  * New upstream.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Thu, 02 Feb 2023 10:34:38 +0100

libpdb-redo (3.0.4-3) unstable; urgency=medium

  * Fix dependency on newuoa to 0.1.2-1.
  * Closes: #1025778

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Sat, 07 Jan 2023 14:47:28 +0100

libpdb-redo (3.0.4-2) unstable; urgency=medium

  * Into unstable.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Fri, 30 Dec 2022 09:12:03 +0100

libpdb-redo (3.0.4-1) experimental; urgency=medium

  * New upstream release.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Wed, 23 Nov 2022 14:08:29 +0100

libpdb-redo (2.0.3-1) unstable; urgency=medium

  * New upstream release.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Thu, 03 Feb 2022 11:29:02 +0100

libpdb-redo (2.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Thu, 03 Feb 2022 10:07:32 +0100

libpdb-redo (2.0.1-3) unstable; urgency=medium

  * Transition, Closes: #1004798

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Wed, 02 Feb 2022 08:58:20 +0100

libpdb-redo (2.0.1-2) experimental; urgency=medium

  * Set correct dependency for libcifpp

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Wed, 19 Jan 2022 13:06:22 +0100

libpdb-redo (2.0.1-1) experimental; urgency=medium

  [ Andrius Merkys ]
  * Team upload.

  [ Maarten L. Hekkelman ]
  * New upstream release

 -- Andrius Merkys <merkys@debian.org>  Tue, 09 Nov 2021 02:35:39 -0500

libpdb-redo (1.0.2-3) unstable; urgency=medium

  * Update dependency to libzeep, libcifpp

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Tue, 02 Nov 2021 07:35:14 +0100

libpdb-redo (1.0.2-2) unstable; urgency=medium

  * Added version numbers to the build-dependencies libzeep and libcifpp
  * Increased tolerance for unit-test to work around i386 failure

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Mon, 04 Jan 2021 13:00:54 +0100

libpdb-redo (1.0.2-1) unstable; urgency=medium

  * New upstream version (includes patch from 1.0.1-2)
  * Fixes pkg-config problem for cross-building
    Closes: #978057

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Mon, 04 Jan 2021 08:35:24 +0100

libpdb-redo (1.0.1-2) unstable; urgency=medium

  * Fix for missing check in bondmap

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Tue, 15 Dec 2020 21:02:30 +0100

libpdb-redo (1.0.1-1) unstable; urgency=medium

  * Updated again with the right upstream code, but this time with a new tag

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Mon, 14 Dec 2020 16:01:18 +0100

libpdb-redo (1.0.0-2) unstable; urgency=medium

  * Updated with the right upstream code

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Mon, 14 Dec 2020 14:09:01 +0100

libpdb-redo (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #976727)

 -- Maarten L. Hekkelman <maarten@hekkelman.com>  Mon, 07 Dec 2020 10:50:19 +0100
